#include "hip/hip_runtime.h"

#ifdef SLDA
#undef SLDA
#endif

#define SLDA(N)  (2*(((N)+1)/2))
//#define SLDA(N)  (N)

template<typename T, int N>
__global__
__launch_bounds__(N)
void
gemm_batched_smallsq_kernel3(
        T** dA_array,
        T** dB_array,
        T** dC_array,
        int batchCount)
{
    extern __shared__ float shared_data[];
    T* sdata = (T*)shared_data;

    const int tx = threadIdx.x;
    const int tz = threadIdx.z;
    const int bx = blockIdx.x;

    const int slda = SLDA(N);

    const int batchid = bx * blockDim.z + tz;
    if(batchid >= batchCount) return;

    const T* __restrict__ dA = dA_array[batchid];
    const T* __restrict__ dB = dB_array[batchid];
          T* __restrict__ dC = dC_array[batchid];

    T rA[ N ] = {0};
    T rTmp = 0, rC = 0;

    T* sB = (T*)(sdata);
    sB += tz * slda * N;

    // read A & B
    #pragma unroll
    for(int j = 0; j < N; j++) {
        rA[j] = dA[j * N + tx];
        sB[j * slda + tx] = dB[j * N + tx];
    }
    __syncthreads();

    // multiply
    for(int j = 0; j < N; j++) {
        rTmp = 0;
        #pragma unroll
        for(int k = 0; k < N; k++){
            rTmp += rA[k] * sB[j * slda + k];
        }
        rC = rTmp;
        dC[j * N + tx] = rC;
    }
}


template<typename T>
int gemm_batched_smallsq_3(
    int n,
    T** dA_array,
    T** dB_array,
    T** dC_array,
    int batchCount, hipStream_t stream )
{
    if( n < 0 || n > 32){
        printf("Only square sizes of up to 32 are supported\n");
        return -1;
    }

    const int slda = SLDA(n);
    int ntcol  = max(1, 64 / n);
    int shmem  = ( 1 * slda * n ) * sizeof(T);
                shmem *= ntcol;

    const int nblocks = (batchCount+ntcol-1) /  ntcol;
    dim3 grid(nblocks, 1, 1);
    dim3 threads(n, 1, ntcol);

    switch(n){
        case  1: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T, 1>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  2: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T, 2>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  3: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T, 3>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  4: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T, 4>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  5: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T, 5>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  6: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T, 6>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  7: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T, 7>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  8: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T, 8>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  9: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T, 9>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 10: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,10>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 11: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,11>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 12: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,12>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 13: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,13>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 14: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,14>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 15: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,15>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 16: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,16>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 17: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,17>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 18: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,18>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 19: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,19>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 20: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,20>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 21: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,21>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 22: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,22>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 23: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,23>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 24: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,24>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 25: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,25>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 26: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,26>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 27: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,27>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 28: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,28>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 29: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,29>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 30: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,30>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 31: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,31>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 32: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel3<T,32>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        default:;
    }

    return 0;
}
