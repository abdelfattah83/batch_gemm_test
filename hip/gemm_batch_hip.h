#include "hip/hip_runtime.h"

#define SLDA(N)  (2*(((N)+1)/2))
//#define SLDA(N)  (N+1)

template<typename T, int N>
__global__ 
__launch_bounds__(N*N)
void
gemm_batched_smallsq_kernel(
        T** dA_array,
        T** dB_array,
        T** dC_array,
        int batchCount)
{
    extern __shared__ float shared_data[];
    T* sdata = (T*)shared_data;

    const int tx = threadIdx.x;
    const int ty = threadIdx.y;
    const int tz = threadIdx.z;
    const int bx = blockIdx.x;
    const int slda = SLDA(N);

    const int batchid = bx * blockDim.z + tz;
    if(batchid >= batchCount) return;

    const T* __restrict__ dA = dA_array[batchid];
    const T* __restrict__ dB = dB_array[batchid];
          T* __restrict__ dC = dC_array[batchid];

    T rC = 0;

    T* sA = (T*)(sdata);
    T* sB = (T*)(sdata + blockDim.z * slda * N);

    sA += tz * slda * N;
    sB += tz * slda * N;

    // read A & B
    sA[ty * slda + tx] = dA[ty * N + tx];
    sB[ty * slda + tx] = dB[ty * N + tx];
    __syncthreads();

    // multiply
    rC = 0;
    #pragma unroll
    for(int j = 0; j < N; j++){
        rC += sA[j * slda + tx] * sB[ty * slda + j];
    }

    // write from rC
    dC[ty * N + tx] = rC;
}


template<typename T>
int gemm_batched_smallsq(
    int n,
    T** dA_array,
    T** dB_array,
    T** dC_array,
    int batchCount, hipStream_t stream )
{
    if( n < 0 || n > 32){
        printf("Only square sizes of up to 32 are supported\n");
        return -1;
    }

    const int slda = SLDA(n);
    int ntcol  = max(1, 64 / (n*n));
    int shmem  = ( 2 * slda * n ) * sizeof(T);
                shmem *= ntcol;

    const int nblocks = (batchCount+ntcol-1) /  ntcol;
    dim3 grid(nblocks, 1, 1);
    dim3 threads(n, n, ntcol);

    switch(n){
        case  1: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T, 1>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  2: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T, 2>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  3: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T, 3>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  4: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T, 4>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  5: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T, 5>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  6: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T, 6>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  7: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T, 7>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  8: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T, 8>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case  9: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T, 9>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 10: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,10>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 11: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,11>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 12: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,12>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 13: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,13>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 14: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,14>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 15: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,15>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 16: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,16>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 17: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,17>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 18: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,18>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 19: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,19>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 20: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,20>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 21: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,21>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 22: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,22>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 23: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,23>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 24: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,24>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 25: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,25>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 26: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,26>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 27: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,27>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 28: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,28>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 29: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,29>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 30: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,30>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 31: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,31>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        case 32: hipLaunchKernelGGL(HIP_KERNEL_NAME(gemm_batched_smallsq_kernel<T,32>), dim3(grid), dim3(threads), shmem, stream, dA_array, dB_array, dC_array, batchCount); break;
        default:;
    }

    return 0;
}
