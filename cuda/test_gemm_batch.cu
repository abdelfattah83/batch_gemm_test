#include<stdio.h>
#include<stdlib.h>
#include <sys/time.h>
#include<vector>
#include<omp.h>
#include<cuda_runtime.h>
#include"gemm_batch.h"
#include"gemm_batch_k2.h"
#include"gemm_batch_k3.h"

////////////////////////////////////////////////////////////////////////////////
double wall_time( void )
{
    struct timeval t;
    gettimeofday( &t, NULL );
    return t.tv_sec + t.tv_usec*1e-6;
}

////////////////////////////////////////////////////////////////////////////////
template<typename T>
void simple_cpu_gemm(int n, T* hA, T* hB, T* hC)
{
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            T r = 0;
            for(int k = 0; k < n; k++) {
                r += hA[k * n + i] * hB[ j * n + k];
            }
            hC[j * n + i] = r;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
template<typename T>
void gemm_test(int kernel_id, int n, int batch, double &elapsed_time, double &performance, double &maxerror)
{
    if(kernel_id < 1 || kernel_id > 3) {
        kernel_id = 1; // default to 1st kernel
    }

    size_t size = batch * n * n;
    const double nd    = (double)n;
    const double flops = (double)batch * 2 * nd * nd * nd / 1e9;

    // alloc host
    std::vector<T> hA( size );
    std::vector<T> hB( size );
    std::vector<T> hC( size );
    std::vector<T> hC_gpu( size );

    // init cpu data
    unsigned int seed;
    #pragma omp parallel private(seed)
    {
        seed = omp_get_thread_num();
        #pragma omp for private(seed)
        for(int i = 0; i < size; ++i) {
            hA[i] = rand_r(&seed) / (T)RAND_MAX;
            hB[i] = rand_r(&seed) / (T)RAND_MAX;
            hC[i] = rand_r(&seed) / (T)RAND_MAX;
            hC_gpu[i] = hC[i];
        }
    }

    // alloc device
    T *dA=NULL, *dB=NULL, *dC=NULL;
    cudaMalloc( (void**)&dA, size * sizeof(T) );
    cudaMalloc( (void**)&dB, size * sizeof(T) );
    cudaMalloc( (void**)&dC, size * sizeof(T) );

    // send data
    cudaMemcpy( dA, hA.data(), size * sizeof(T), cudaMemcpyHostToDevice );
    cudaMemcpy( dB, hB.data(), size * sizeof(T), cudaMemcpyHostToDevice );
    cudaMemcpy( dC, hC.data(), size * sizeof(T), cudaMemcpyHostToDevice );

    // setup pointers
    T **dA_array=NULL, **dB_array=NULL, **dC_array=NULL;
    std::vector<T*> hA_array( batch );
    std::vector<T*> hB_array( batch );
    std::vector<T*> hC_array( batch );
    cudaMalloc( (void**)&dA_array, batch * sizeof(T*) );
    cudaMalloc( (void**)&dB_array, batch * sizeof(T*) );
    cudaMalloc( (void**)&dC_array, batch * sizeof(T*) );

    for(int i = 0; i < batch; i++) {
        hA_array[i] = dA + i * n * n;
        hB_array[i] = dB + i * n * n;
        hC_array[i] = dC + i * n * n;
    }
    cudaMemcpy(dA_array, hA_array.data(), batch * sizeof(T*), cudaMemcpyHostToDevice);
    cudaMemcpy(dB_array, hB_array.data(), batch * sizeof(T*), cudaMemcpyHostToDevice);
    cudaMemcpy(dC_array, hC_array.data(), batch * sizeof(T*), cudaMemcpyHostToDevice);

    // create stream/event
    cudaStream_t stream;
    cudaStreamCreate( &stream );

    // launch
    elapsed_time = wall_time();

    if(kernel_id == 1) {
        gemm_batched_smallsq<T>(n, dA_array, dB_array, dC_array, batch, stream );
    }
    else if (kernel_id == 2) {
        gemm_batched_smallsq_2<T>(n, dA_array, dB_array, dC_array, batch, stream );
    }
    else if (kernel_id == 3) {
        gemm_batched_smallsq_3<T>(n, dA_array, dB_array, dC_array, batch, stream );
    }

    cudaStreamSynchronize( stream );

    elapsed_time = wall_time() - elapsed_time;
    performance = flops / elapsed_time;

    // get data
    cudaMemcpy( hC_gpu.data(), dC, size * sizeof(T), cudaMemcpyDeviceToHost );

    // cpu compute
    for(int i = 0; i < batch; i++) {
        hA_array[i] = hA.data() + i * n * n;
        hB_array[i] = hB.data() + i * n * n;
        hC_array[i] = hC.data() + i * n * n;
    }

    #pragma omp parallel for schedule(static)
    for(int ib = 0; ib < batch; ib++) {
        simple_cpu_gemm<T>(n, hA_array[ib], hB_array[ib], hC_array[ib]);
    }

    // compute error
    double error = 0;
    #pragma omp parallel for reduction(max:error)
    for(int i = 0; i < batch*n*n; i++) {
        double err = std::abs(hC[i] - hC_gpu[i]) / std::abs(hC[i]);
        error = std::max(error, err);
    }
    maxerror = error;

    // free data
    cudaFree( dA );
    cudaFree( dB );
    cudaFree( dC );
    cudaFree( dA_array );
    cudaFree( dB_array );
    cudaFree( dC_array );

    cudaStreamDestroy( stream );
}


int main( int argc, char* argv[] )
{
    char p            = 's';
    int  n            = 16;
    int  batch        = 1000;
    int  print_header = 0;
    int  niter        = 1;

    if( argc > 1 ) {
        p = *argv[1];
    }

    if( argc > 2 ) {
        n = atoi( argv[2] );
    }

    if( argc > 3 ) {
        batch = atoi( argv[3] );
    }

    if( argc > 4 ) {
        niter = atoi( argv[4] );
    }

    if( argc > 5 ) {
        print_header = atoi( argv[5] );
    }

    double elapsed_time=0, performance=0, maxerror = 0;

    // warmup runs
    /*
    for(int kid = 1; kid <= 3; kid++) {
        for(int i = 1; i < 32; i++) {
            gemm_test<float>(kid, i, 10, elapsed_time, performance, maxerror);
            gemm_test<double>(kid, i, 10, elapsed_time, performance, maxerror);
        }
    }
    */

    elapsed_time=0; performance=0; maxerror = 0;

    if(print_header != 0) {
        printf("# Kernel 1: NxN threads per GEMM, A and B are in shared memory. One sgpr per thread for C\n");
        printf("# Kernel 2: Nx1 threads per GEMM, A and B are in shared memory. Two sgpr per thread for C\n");
        printf("# Kernel 3: Nx1 threads per GEMM, A in reg, B in shared memory. N sgpr's per thread for C\n");
        printf("#                                      Kernel 1                           Kernel 2                        Kernel 3               \n");
        printf("# Precision      N  Batch  [ time (ms),   Gflop/s,   error ] [ time (ms),   Gflop/s,   error ] [ time (ms),   Gflop/s,   error ] \n");
        printf("# ------------------------------------------------------------------------------------------------------------------------------ \n");
    }

    for(int iter = 0; iter < niter; ++iter) {
        if( p == 's' ) {
            printf("  %9s  %5d  %5d  ", "Single", n, batch);

            // kernel 1
            gemm_test<float>(1, n, batch, elapsed_time, performance, maxerror);
            printf("  %9.2f  %9.2f  %6.1e   ", elapsed_time*1000., performance, maxerror);

            // kernel 2
            gemm_test<float>(2, n, batch, elapsed_time, performance, maxerror);
            printf("  %9.2f  %9.2f  %6.1e   ", elapsed_time*1000., performance, maxerror);

            // kernel 3
            gemm_test<float>(3, n, batch, elapsed_time, performance, maxerror);
            printf("  %9.2f  %9.2f  %6.1e\n", elapsed_time*1000., performance, maxerror);
        }
        else if( p == 'd' ) {
            printf("  %9s  %5d  %5d  ", "Double", n, batch);

            // kernel 1
            gemm_test<double>(1, n, batch, elapsed_time, performance, maxerror);
            printf("  %9.2f  %9.2f  %6.1e   ", elapsed_time*1000., performance, maxerror);

            // kernel 2
            gemm_test<double>(2, n, batch, elapsed_time, performance, maxerror);
            printf("  %9.2f  %9.2f  %6.1e   ", elapsed_time*1000., performance, maxerror);

            // kernel 3
            gemm_test<double>(3, n, batch, elapsed_time, performance, maxerror);
            printf("  %9.2f  %9.2f  %6.1e\n", elapsed_time*1000., performance, maxerror);
        }
        else {
            continue;
        }
    }
}
