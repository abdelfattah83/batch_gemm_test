
nstart=2
nstop=32
nstep=1
batch=100000
repeat=10
header=1

for p in s d
do
	output=${p}gemm_batch_${batch}_cuda.txt
	rm -f $output
	touch $output
	header=1
	for((n=$nstart;n<=$nstop;n=n+$nstep))
	do
		echo "Running for (p,n) = ("$p","$n")"
		./main_cuda $p $n $batch $repeat $header >> $output
		echo "#" >> $output
		header=0
	done
done
