#ifdef SLDA
#undef SLDA
#endif

#define SLDA(N)  (2*(((N)+1)/2))

template<typename T, int N>
__global__ void
gemm_batched_smallsq_kernel2(
        T** dA_array,
        T** dB_array,
        T** dC_array,
        int batchCount)
{
    extern __shared__ float shared_data[];
    T* sdata = (T*)shared_data;

    const int tx = threadIdx.x;
    const int tz = threadIdx.z;
    const int bx = blockIdx.x;

	const int slda = SLDA(N);
    const int batchid = bx * blockDim.z + tz;
    if(batchid >= batchCount) return;

    const T* __restrict__ dA = dA_array[batchid];
    const T* __restrict__ dB = dB_array[batchid];
          T* __restrict__ dC = dC_array[batchid];

    T rTmp = 0, rC = 0;

    T* sA = (T*)(sdata);
    T* sB = (T*)(sdata + blockDim.z * slda * N);

    sA += tz * slda * N;
    sB += tz * slda * N;

    // read A & B
    #pragma unroll
    for(int j = 0; j < N; j++) {
        sA[j * slda + tx] = dA[j * N + tx];
        sB[j * slda + tx] = dB[j * N + tx];
    }
    __syncthreads();

    // multiply
    for(int j = 0; j < N; j++) {
        rTmp = 0;
        #pragma unroll
        for(int k = 0; k < N; k++){
            rTmp += sA[k * slda + tx] * sB[j * slda + k];
        }
        rC = rTmp;
        dC[j * N + tx] = rC;
    }
}


template<typename T>
int gemm_batched_smallsq_2(
    int n,
    T** dA_array,
    T** dB_array,
    T** dC_array,
    int batchCount, cudaStream_t stream )
{
    if( n < 0 || n > 32){
        printf("Only square sizes of up to 32 are supported\n");
        return -1;
    }

    const int slda = SLDA(n);
	int ntcol  = max(1, 32 / n);
    int shmem  = ( 2 * slda * n ) * sizeof(T);
                shmem *= ntcol;

    const int nblocks = (batchCount+ntcol-1) /  ntcol;
    dim3 grid(nblocks, 1, 1);
    dim3 threads(n, 1, ntcol);

    switch(n){
        case  1: gemm_batched_smallsq_kernel2<T, 1><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case  2: gemm_batched_smallsq_kernel2<T, 2><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case  3: gemm_batched_smallsq_kernel2<T, 3><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case  4: gemm_batched_smallsq_kernel2<T, 4><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case  5: gemm_batched_smallsq_kernel2<T, 5><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case  6: gemm_batched_smallsq_kernel2<T, 6><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case  7: gemm_batched_smallsq_kernel2<T, 7><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case  8: gemm_batched_smallsq_kernel2<T, 8><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case  9: gemm_batched_smallsq_kernel2<T, 9><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 10: gemm_batched_smallsq_kernel2<T,10><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 11: gemm_batched_smallsq_kernel2<T,11><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 12: gemm_batched_smallsq_kernel2<T,12><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 13: gemm_batched_smallsq_kernel2<T,13><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 14: gemm_batched_smallsq_kernel2<T,14><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 15: gemm_batched_smallsq_kernel2<T,15><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 16: gemm_batched_smallsq_kernel2<T,16><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 17: gemm_batched_smallsq_kernel2<T,17><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 18: gemm_batched_smallsq_kernel2<T,18><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 19: gemm_batched_smallsq_kernel2<T,19><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 20: gemm_batched_smallsq_kernel2<T,20><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 21: gemm_batched_smallsq_kernel2<T,21><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 22: gemm_batched_smallsq_kernel2<T,22><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 23: gemm_batched_smallsq_kernel2<T,23><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 24: gemm_batched_smallsq_kernel2<T,24><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 25: gemm_batched_smallsq_kernel2<T,25><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 26: gemm_batched_smallsq_kernel2<T,26><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 27: gemm_batched_smallsq_kernel2<T,27><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 28: gemm_batched_smallsq_kernel2<T,28><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 29: gemm_batched_smallsq_kernel2<T,29><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 30: gemm_batched_smallsq_kernel2<T,30><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 31: gemm_batched_smallsq_kernel2<T,31><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        case 32: gemm_batched_smallsq_kernel2<T,32><<<grid, threads, shmem, stream>>>(dA_array, dB_array, dC_array, batchCount); break;
        default:;
    }

    return 0;
}
